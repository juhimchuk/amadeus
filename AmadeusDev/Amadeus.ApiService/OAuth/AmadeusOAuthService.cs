﻿using Amadeus.IApiService;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;

namespace Amadeus.ApiService.OAuth
{
    public class AmadeusOAuthService : IAmadeusOAuthService
    {
        public string GetAmadeusToken()
        {
            var pairs = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>( "grant_type", ConfigurationManager.AppSettings["GrantType"] ),
                new KeyValuePair<string, string>( "client_id", ConfigurationManager.AppSettings["ClientId"] ),
                new KeyValuePair<string, string>( "client_secret", ConfigurationManager.AppSettings["ClientSecret"] )
            };
            var content = new FormUrlEncodedContent(pairs);
            using (var client = new HttpClient())
            {
                var response = client.PostAsync(ConfigurationManager.AppSettings["BaseUrl"] + "security/oauth2/token", content).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                var token = JsonConvert.DeserializeObject<Dictionary<string, string>>(result)["access_token"];
                return token;
            }
        }
    }
}
