﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.ApiService
{
    public class CreateClient
    {

        public CreateClient()
        {
            AppPath = ConfigurationManager.AppSettings["BaseUrl"];
            MediaType = new MediaTypeWithQualityHeaderValue("application/vnd.amadeus+json");
        }

        public string AppPath { get; set; }
        public MediaTypeWithQualityHeaderValue MediaType { get; set; }
    }
}
