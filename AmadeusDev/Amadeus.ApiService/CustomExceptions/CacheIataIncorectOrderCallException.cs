﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.ApiService.CustomExceptions
{
    public class CacheIataIncorectOrderCallException : ApplicationException
    {
        public CacheIataIncorectOrderCallException() { }

        public CacheIataIncorectOrderCallException(string message) : base(message) { }

        public CacheIataIncorectOrderCallException(string message, Exception inner) : base(message, inner) { }

        protected CacheIataIncorectOrderCallException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
