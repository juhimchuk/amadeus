﻿using Amadeus.IApiService.IATA;
using Amadeus.Model.IATA;
using Refit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.ApiService.IATA
{
    public class RestIataCodesService : IIataCodeService
    {
        readonly string _apiKey;
        readonly IIataCodesServiceRefit _iataApi;

        public RestIataCodesService(string baseUrl, string apiKey)
        {
            _iataApi = RestService.For<IIataCodesServiceRefit>(baseUrl);
            _apiKey = apiKey;
        }

        public async Task<AirportInfo> GetAirportInfoByCode(string airportCode)
        {
            var airportInfos = await _iataApi.GetAirportInfos(_apiKey, airportCode);

            return airportInfos.Response.First().AsDomain();
        }

        public async Task<Dictionary<string, AirportInfo>> GetAllAirportInfos()
        {
            var airportInfos = await _iataApi.GetAllAirportInfos(_apiKey);
            return airportInfos
                .Response
                .ToDictionary(ai => ai.Code, ai => ai.AsDomain());
        }

        public async Task<Dictionary<string, CityInfo>> GetAllCityInfos()
        {
            return (await _iataApi.GetAllCityInfos(_apiKey))
                .Response
                .ToDictionary(ai => ai.Code, ai => ai.AsDomain());
        }

        public async Task<CityInfo> GetCityInfoByCode(string cityCode)
        {
            var cityInfos = await _iataApi.GetCityInfos(_apiKey, cityCode);

            return cityInfos.Response.First().AsDomain();
        }

        public async Task<Dictionary<string, AirlineInfo>> GetAllAirlinesInfos()
        {
            var airlinesInfos = await _iataApi.GetAllAirlinesInfos(_apiKey);
            var airlines = new Dictionary<string, AirlineInfo>();
            var codes = new List<string>();
            foreach (var air in airlinesInfos.Response)
            {
                if (codes.Contains(air.Code))
                {
                    Debug.WriteLine("More than one code - " + air.Code);
                }
                else
                {
                    if (air.Code != null)
                    {
                        airlines.Add(air.Code, air.AsDomain());
                        codes.Add(air.Code);
                    }
                }
            }
            return airlines;

        }
    }
}
