﻿using Amadeus.ApiService.CustomExceptions;
using Amadeus.IApiService.IATA;
using Domain.GeneralModels;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Amadeus.ApiService.IATA
{
    public class CachingAiroportCodeService : ICachingAiroportCodeService
    {
        List<AiroportCodeModel> _codeInfo;
        public bool IsCalledInit { get; set; }

        readonly IAiroportCodeService _codesService;

        public CachingAiroportCodeService(IAiroportCodeService codesService)
        {
            _codesService = codesService;
        }

        public async Task Init()
        {
            _codeInfo = _codesService.GetAll().ToList();
            IsCalledInit = true;
        }

        public AiroportCodeModel GetInfo(string code)
        {
            CheckCalledInit("GetInfo");
            var result = _codeInfo.FirstOrDefault(c => c.IataCode.ToUpper() == code.ToUpper());

            Debug.WriteLineIf(result == null,code);

            return result;
        }

        void CheckCalledInit(string methodName)
        {
            if (!IsCalledInit)
            {
                throw new CacheIataIncorectOrderCallException($"Before call {methodName} need call Init method");
            }
        }
    }
}
