﻿using Amadeus.ApiService.CustomExceptions;
using Amadeus.IApiService.IATA;
using Amadeus.Model.IATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.ApiService.IATA
{
    public class CachingIataCodesService : ICachingIataCodesService
    {
        Dictionary<string, AirlineInfo> _airlineInfoDictionary;
        Dictionary<string, AirportInfo> _airportsInfoDictionary;
        Dictionary<string, CityInfo> _citiesInfoDictionary;
        public bool IsCalledInit { get; set; }
        readonly IIataCodeService _iataCodesService;

        public CachingIataCodesService(IIataCodeService iataCodesService)
        {
            _iataCodesService = iataCodesService;
        }

        public async Task Init()
        {
            _airlineInfoDictionary = await _iataCodesService.GetAllAirlinesInfos();
            _airportsInfoDictionary = await _iataCodesService.GetAllAirportInfos();
            _citiesInfoDictionary = await _iataCodesService.GetAllCityInfos();
            IsCalledInit = true;
        }

        public string GetAirportName(string code)
        {
            CheckCalledInit("GetAirportName");
            return _airportsInfoDictionary.ContainsKey(code)
                ? _airportsInfoDictionary[code].Name
                : null;
        }

        public string GetAirlineName(string code)
        {
            CheckCalledInit("GetAirlineName");
            return _airlineInfoDictionary.ContainsKey(code)
                ? _airlineInfoDictionary[code].Name
                : null;
        }

        public string GetCityName(string code)
        {
            CheckCalledInit("GetCityName");
            return _citiesInfoDictionary.ContainsKey(code)
                ? _citiesInfoDictionary[code].Name
                : null;
        }

        void CheckCalledInit(string methodName)
        {
            if (!IsCalledInit)
            {
                throw new CacheIataIncorectOrderCallException($"Before call {methodName} need call Init method");
            }
        }
    }
}
