﻿using Amadeus.IApiService.IATA;
using Amadeus.IApiService.IRepository;
using Domain.GeneralModels;
using System.Collections.Generic;

namespace Amadeus.ApiService.IATA
{
    public class AiroportCodeService : IAiroportCodeService
    {
        private readonly IAiroportCodeRepository _airoportCode;

        public AiroportCodeService(IAiroportCodeRepository airoportCode)
        {
            _airoportCode = airoportCode;
        }

        public IEnumerable<AiroportCodeModel> GetAll()
        {
            return _airoportCode.GetAll();
        }
    }
}
