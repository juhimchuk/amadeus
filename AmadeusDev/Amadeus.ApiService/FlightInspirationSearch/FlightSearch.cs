﻿using Amadeus.IApiService;
using Amadeus.IApiService.IATA;
using Amadeus.IApiService.IFlightInspirationSearch;
using Amadeus.Model.AmadeusModels;
using Domain.FlightInspirationModels.ResponseModels;
using Domain.FlightInspirationModels.SearchModels;
using Domain.GeneralModels;
using Newtonsoft.Json;
using Polly;
using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Amadeus.ApiService.FlightInspirationSearch
{
    public class FlightSearch : IFlightSearch
    {
        private readonly CreateClient _setting;
        private readonly ICachingAiroportCodeService _cacheService;
        private readonly IAmadeusOAuthService _oauthService;

        public FlightSearch(ICachingAiroportCodeService cacheService, IAmadeusOAuthService oauth)
        {
            _setting = new CreateClient();
            _cacheService = cacheService;
            _oauthService = oauth;
        }

        public FlightResponseModel InspirationSearch(SearchModel search)
        {
            using (var client = new HttpClient())
            {
                var flights = Policy
                    .Handle<Exception>()
                    .Retry(3)
                    .Execute(() =>
                   {
                       client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", search.Token);
                       var responses = client.GetAsync(_setting.AppPath + $"shopping/flight-destinations?origin={search.Origin}&departureDate={search.DepartureDateFrom.ToString("yyyy'-'MM'-'dd")},{search.DepartureDateTo.ToString("yyyy'-'MM'-'dd")}&oneWay={search.IsOneWay}&nonStop={search.IsNonStop}&viewBy=DESTINATION").Result;

                       var flight = GetResult(responses);
                       if (flight.Errors != null)
                       {
                           if (flight.Errors.Any(a => a.Status == 401))
                           {
                               ConfigurationManager.AppSettings["AmadeusToken"] = search.Token = _oauthService.GetAmadeusToken();
                               throw new Exception();
                           }
                       }
                       return flight;
                   });
                return flights;
            }
        }

        public FlightResponseModel CheapestSearch(SearchModel search)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", search.Token);
                var response = client.GetAsync(_setting.AppPath + $"shopping/flight-dates?origin={search.Origin}&destination={search.Destination}&departureDate={search.DepartureDateFrom.ToString("yyyy'-'MM'-'dd")},{search.DepartureDateTo.ToString("yyyy'-'MM'-'dd")}&oneWay={search.IsOneWay}&nonStop={search.IsNonStop}&viewBy=DURATION").Result;
                var flight = GetResult(response);
                return flight;
            }
        }

        public FlightResponseModel GetResult(HttpResponseMessage response)
        {
            var content = response.Content.ReadAsStringAsync().Result;
            var flight = Map(JsonConvert.DeserializeObject<FlightInspirationAmadeusResponseModel>(content));
            return flight;
        }
        
        private FlightResponseModel Map(FlightInspirationAmadeusResponseModel model)
        {
            return new FlightResponseModel
            {
                Data = model.Data?.Select(m=> Map(m)).ToList(),
                Errors = model.Errors?.Select(e => MapError(e)).ToList()
            };
        }
        private ResponseErrorModel MapError(AmadeusErrorModel model)
        {
            return model == null ? null : new ResponseErrorModel
            {
                Code = Convert.ToInt32(model.Code),
                Status = Convert.ToInt32(model.Status),
                Title = model.Title,
                Detail = model.Detail
            };
        }

        private FlightInformationModel Map(AmadeusDataSearchModel model)
        {
            return model == null ? null : new FlightInformationModel
            {
                Type = model.Type,
                Origin = GetAiroportModel(model.Origin),
                Destination = GetAiroportModel(model.Destination),
                DepartureDate = Convert.ToDateTime(model.DepartureDate),
                ReturnDate = GetReturnDate(model.ReturnDate),
                Price = Convert.ToDecimal(model.Price.Total.Replace('.',',')),
                Links = GetExtraLinksModel(model.Links)
            };
        }

        private DateTime? GetReturnDate(string returnDate)
        {
            var date = new DateTime();
            return DateTime.TryParse(returnDate, out date) ? date : (DateTime?)null;
        }

        private ExtraLinksModel GetExtraLinksModel(LinksDataAmadeusModel model)
        {
            return new ExtraLinksModel
            {
                FlightDestinations = model.FlightDestinations,
                SearchDate = model.FlightDates,
                SearchTickets = model.FlightOffers
            };
        }

        private AiroportModel GetAiroportModel(string code)
        {
            var info= _cacheService.GetInfo(code);
            return new AiroportModel
            {
                Code = info.IataCode,
                Name = info.AiroportName,
                City = new CityModel
                {
                    Name = info.CityName,
                    CountryName = info.CountryName
                }
            };
        }


    }
}
