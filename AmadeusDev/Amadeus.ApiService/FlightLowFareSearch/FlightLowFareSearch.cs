﻿using Amadeus.IApiService.IATA;
using Amadeus.IApiService.IFlightInspirationSearch;
using Amadeus.Model.AmadeusModels;
using Amadeus.Model.AmadeusModels.LowFareAmadeusModels;
using Domain.GeneralModels;
using Domain.LowFareModels.ResponseModels;
using Domain.LowFareModels.SearchModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Amadeus.ApiService.FlightLowFareSearch
{
    public class FlightLowFareSearch : IFlightLowFareSearch
    {
        private readonly CreateClient _setting;
        private readonly ICachingAiroportCodeService _cacheService;

        private Dictionary<string, string> _airCraft;
        private Dictionary<string, string> _carriersCode;

        public FlightLowFareSearch(ICachingAiroportCodeService cacheService)
        {
            _setting = new CreateClient();
            _cacheService = cacheService;
        }

        public ResponseLowFareModel Search(LowFareSearchModel search)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", search.Token);
                var response = client.GetAsync(_setting.AppPath + $"shopping/flight-offers?origin={search.Origin}&destination={search.Destination}&departureDate={search.DepartureDate.ToString("yyyy'-'MM'-'dd")}&returnDate={search.ReturnDate.ToString("yyyy'-'MM'-'dd")}&adults={search.Passengers.FirstOrDefault(p=>p.TypePassenger==TypePassenger.ADT).NumberOfPassengers}&children={search.Passengers.FirstOrDefault(p => p.TypePassenger == TypePassenger.CH).NumberOfPassengers}&nonStop={search.IsNonStop}").Result;
                var flights = GetResult(response);
                return flights;
            }
        }

        public ResponseLowFareModel GetResult(HttpResponseMessage response)
        {
            var content = response.Content.ReadAsStringAsync().Result;
            var flights = Map(JsonConvert.DeserializeObject<AmadeusResponseModel>(content));
            return flights;
        }

        private ResponseLowFareModel Map(AmadeusResponseModel model)
        {
            _airCraft = model.Dictionaries.AirCraft;
            _carriersCode = model.Dictionaries.CarriersCode;
            return new ResponseLowFareModel
            {
                Data = model.Data?.Select(m => Map(m)).ToList(),
                Errors = model.Errors?.Select(e => MapError(e)).ToList()
            };
        }
        private ResponseErrorModel MapError(AmadeusErrorModel model)
        {
            return model == null ? null : new ResponseErrorModel
            {
                Code = Convert.ToInt32(model.Code),
                Status = Convert.ToInt32(model.Status),
                Title = model.Title,
                Detail = model.Detail
            };
        }

        private DataModel Map(AmadeusDataModel model)
        {
            return model == null ? null : new DataModel
            {
                Type = model.Type,
                Id = model.Id,
                OfferItems = model.OfferItems.Select(o=>Map(o))
            };
        }

        private OfferItemsModel Map(AmadeusOfferItemsModel model)
        {
            return new OfferItemsModel
            {
                Price = GetPrice(model.Price),
                PricePerAdult = GetPrice(model.PricePerAdult),
                PricePerChild = GetPrice(model.PricePerChild),
                Sevices = model.Sevices.Select(s=> Map(s))
            };
        }

        private ServiceModel Map(AmadeusServiceModel model)
        {
            return new ServiceModel
            {
                Segments = model.Segments.Select(s=> Map(s))
            };
        }

        private SegmentModel Map(AmadeusSegmentModel model)
        {
            return new SegmentModel
            {
                FlightSegment = GetFlightSegment(model.FlightSegment),
                PricingDetailPerAdult = GetPriceDetailModel(model.PricingDetailPerAdult),
                PricingDetailPerChild = GetPriceDetailModel(model.PricingDetailPerChild)
            };
        }

        private PricingDetailModel GetPriceDetailModel(AmadeusPricingDetailModel model)
        {
            return model == null ? null : new PricingDetailModel
            {
                TravelClass = model.TravelClass,
                FareClass = model.FareClass,
                Availability = Convert.ToInt32(model.Availability),
                FareBasis = model.FareBassis
            };
        }

        private FlightSegmentModel GetFlightSegment(AmadeusFlightSegmentModel model)
        {
            return new FlightSegmentModel
            {
                Departure = GetAiroportModel(model.Departure),
                Arrival = GetAiroportModel(model.Arrival),
                AirCraft = GetAirCraftModel(model.AirCraft),
                Operating = GetOpModel(model.Operating)
            };

        }

        private LowFareAiroportModel GetAiroportModel(AmadeusAiroportModel model)
        {
            int ter;
            return new LowFareAiroportModel
            {
                Airoport = GetAiroportModel(model.IataCode),
                Terminal = Int32.TryParse(model.Terminal, out ter) ? ter : (int?)null,
                ArrivalTime = Convert.ToDateTime(model.At)
            };

        }

        private AirCraftModel GetAirCraftModel(AmadeusAirCraftModel model)
        {
            return new AirCraftModel
            {
               Code = model.Code,
               Name = _airCraft[model.Code]
            };

        }

        private AiroportModel GetAiroportModel(string code)
        {
            var info = _cacheService.GetInfo(code);
            return new AiroportModel
            {
                Code = info.IataCode,
                Name = info.AiroportName,
                City = new CityModel
                {
                    Name = info.CityName,
                    CountryName = info.CountryName
                }
            };
        }

        private PriceModel GetPrice(AmadeusDataPriceModel model)
        {
            return model == null ? null : new PriceModel
            {
                Total = GetDecimalPrice(model.Total),
                TotalTaxes = GetDecimalPrice(model.TotalTaxes)
            };
        }

        private OperatingCodeModel GetOpModel(AmadeusOperatingModel model)
        {
            return new OperatingCodeModel
            {
                CarrierCode = model.CarrierCode,
                Number = model.Number,
                Name = _carriersCode[model.CarrierCode]
            };
        }

        private decimal GetDecimalPrice(string price)
        {
            return Convert.ToDecimal(price.Replace('.', ','));
        }

    }
}
