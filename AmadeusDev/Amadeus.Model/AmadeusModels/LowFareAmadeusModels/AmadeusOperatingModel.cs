﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels.LowFareAmadeusModels
{
    public class AmadeusOperatingModel
    {
        [JsonProperty(PropertyName = "carrierCode")]
        public string CarrierCode { get; set; }

        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

    }
}
