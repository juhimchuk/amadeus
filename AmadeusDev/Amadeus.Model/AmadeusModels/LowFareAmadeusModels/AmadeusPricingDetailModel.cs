﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels.LowFareAmadeusModels
{
    public class AmadeusPricingDetailModel
    {
        [JsonProperty(PropertyName = "travelClass")]
        public string TravelClass { get; set; }

        [JsonProperty(PropertyName = "fareClass")]
        public string FareClass { get; set; }

        [JsonProperty(PropertyName = "availability")]
        public string Availability { get; set; }

        [JsonProperty(PropertyName = "fareBasis")]
        public string FareBassis { get; set; }

    }
}
