﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.Model.AmadeusModels.LowFareAmadeusModels
{
    public class AmadeusDictionaryModel
    {
        [JsonProperty(PropertyName = "carriers")]
        public Dictionary<string, string> CarriersCode { get; set; }

        [JsonProperty(PropertyName = "currencies")]
        public Dictionary<string, string> Currencies { get; set; }

        [JsonProperty(PropertyName = "aircraft")]
        public Dictionary<string, string> AirCraft { get; set; }
    }
}
