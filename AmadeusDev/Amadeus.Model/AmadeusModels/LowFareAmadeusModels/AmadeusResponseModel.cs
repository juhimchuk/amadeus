﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels.LowFareAmadeusModels
{
    public class AmadeusResponseModel
    {
        [JsonProperty(PropertyName = "data")]
        public IEnumerable<AmadeusDataModel> Data { get; set; }

        [JsonProperty(PropertyName = "dictionaries")]
        public AmadeusDictionaryModel Dictionaries { get; set; }

        [JsonProperty(PropertyName = "errors")]
        public IEnumerable<AmadeusErrorModel> Errors { get; set; }
    }
}
