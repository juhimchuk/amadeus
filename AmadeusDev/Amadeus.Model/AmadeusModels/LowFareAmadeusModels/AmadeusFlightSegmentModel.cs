﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels.LowFareAmadeusModels
{
    public class AmadeusFlightSegmentModel
    {
        [JsonProperty(PropertyName = "departure")]
        public AmadeusAiroportModel Departure { get; set; }

        [JsonProperty(PropertyName = "arrival")]
        public AmadeusAiroportModel Arrival { get; set; }

        [JsonProperty(PropertyName = "carrierCode")]
        public string CarrierCode { get; set; }

        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        [JsonProperty(PropertyName = "aircraft")]
        public AmadeusAirCraftModel AirCraft { get; set; }

        [JsonProperty(PropertyName = "operating")]
        public AmadeusOperatingModel Operating { get; set; }

        [JsonProperty(PropertyName = "duration")]
        public string Duration { get; set; }


    }
}
