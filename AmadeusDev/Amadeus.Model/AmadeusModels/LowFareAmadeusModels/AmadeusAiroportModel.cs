﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels.LowFareAmadeusModels
{
    public class AmadeusAiroportModel
    {
        [JsonProperty(PropertyName = "iataCode")]
        public string IataCode { get; set; }

        [JsonProperty(PropertyName = "terminal")]
        public string Terminal { get; set; }

        [JsonProperty(PropertyName = "at")]
        public string At { get; set; }

    }
}
