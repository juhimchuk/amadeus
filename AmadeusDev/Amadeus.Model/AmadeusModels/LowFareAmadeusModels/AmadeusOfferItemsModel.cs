﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels.LowFareAmadeusModels
{
    public class AmadeusOfferItemsModel
    {
        [JsonProperty(PropertyName = "services")]
        public IEnumerable<AmadeusServiceModel> Sevices { get; set; }

        [JsonProperty(PropertyName = "price")]
        public AmadeusDataPriceModel Price { get; set; }

        [JsonProperty(PropertyName = "pricePerAdult")]
        public AmadeusDataPriceModel PricePerAdult { get; set; }

        [JsonProperty(PropertyName = "pricePerChild")]
        public AmadeusDataPriceModel PricePerChild { get; set; }

    }
}
