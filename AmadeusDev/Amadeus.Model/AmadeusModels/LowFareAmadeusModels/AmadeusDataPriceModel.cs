﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels.LowFareAmadeusModels
{
    public class AmadeusDataPriceModel
    {
        [JsonProperty(PropertyName = "total")]
        public string Total { get; set; }

        [JsonProperty(PropertyName = "totalTaxes")]
        public string TotalTaxes { get; set; }
    }
}
