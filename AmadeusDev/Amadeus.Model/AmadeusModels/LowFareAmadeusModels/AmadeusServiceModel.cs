﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels.LowFareAmadeusModels
{
    public class AmadeusServiceModel
    {
        [JsonProperty(PropertyName = "segments")]
        public IList<AmadeusSegmentModel> Segments { get; set; }
    }
}
