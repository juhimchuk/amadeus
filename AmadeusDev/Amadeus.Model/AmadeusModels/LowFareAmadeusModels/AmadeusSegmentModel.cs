﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels.LowFareAmadeusModels
{
    public class AmadeusSegmentModel
    {
        [JsonProperty(PropertyName = "flightSegment")]
        public AmadeusFlightSegmentModel FlightSegment { get; set; }

        [JsonProperty(PropertyName = "pricingDetailPerAdult")]
        public AmadeusPricingDetailModel PricingDetailPerAdult { get; set; }

        [JsonProperty(PropertyName = "pricingDetailPerChild")]
        public AmadeusPricingDetailModel PricingDetailPerChild { get; set; }

    }
}
