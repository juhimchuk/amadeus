﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels
{
    public class LinksDataAmadeusModel
    {
        [JsonProperty(PropertyName = "flightDates")]
        public string FlightDates { get; set; }

        [JsonProperty(PropertyName = "flightDestinations")]
        public string FlightDestinations { get; set; }

        [JsonProperty(PropertyName = "flightOffers")]
        public string FlightOffers { get; set; }
    }
}
