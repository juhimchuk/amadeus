﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels
{
    public class FlightInspirationAmadeusResponseModel
    {
        [JsonProperty(PropertyName = "data")]
        public IEnumerable<AmadeusDataSearchModel> Data { get; set; }

        [JsonProperty(PropertyName = "errors")]
        public IEnumerable<AmadeusErrorModel> Errors { get; set; }
    }
}
