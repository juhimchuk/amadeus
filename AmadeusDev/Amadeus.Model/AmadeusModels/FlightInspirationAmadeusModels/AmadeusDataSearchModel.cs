﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Amadeus.Model.AmadeusModels
{
    public class AmadeusDataSearchModel
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "origin")]
        public string Origin { get; set; }
        [JsonProperty(PropertyName = "destination")]
        public string Destination { get; set; }
        [JsonProperty(PropertyName = "departureDate")]
        public string DepartureDate { get; set; }
        [JsonProperty(PropertyName = "returnDate")]
        public string ReturnDate { get; set; }
        [JsonProperty(PropertyName = "price")]
        public PriceAmadeusModel Price { get; set; }
        [JsonProperty(PropertyName = "links")]
        public LinksDataAmadeusModel Links { get; set; }
    }
}
