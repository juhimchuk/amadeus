﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.Model.IATA
{
    public class CityInfoRefit
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
        [JsonProperty(PropertyName = "country_code")]
        public string CountryCode { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        public CityInfo AsDomain()
        {
            return new CityInfo
            {
                Code = Code,
                Name = Name,
                CountryCode = CountryCode
            };
        }
    }
}
