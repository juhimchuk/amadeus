﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.Model.IATA
{
    /// <summary>
    /// IATA Codes Airport Lookup Response.
    /// Note that the full set of properties populated
    /// only with premium access.
    /// For free access only Code, CountryCode and Name is populated.
    /// </summary>
    public class CityInfo
    {
        public string Code { get; set; }
        public string CountryCode { get; set; }
        public string Name { get; set; }
        public string AlternateNames { get; set; }
        public long Lat { get; set; }
        public long Lon { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
        public int GmtOffset { get; set; }
        public string TchCode { get; set; }
        public int Popularity { get; set; }
        public string GeoNameId { get; set; }
    }
}
