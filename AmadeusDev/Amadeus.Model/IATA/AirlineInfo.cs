﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.Model.IATA
{
    public class AirlineInfo
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
