﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.Model.IATA
{
    /// <summary>
    /// IATA Codes Airport Lookup Response.
    /// Note that the full set of properties populated
    /// only with premium access.
    /// For free access only Code and Name is populated.
    /// </summary>
    public class AirportInfo
    {
        public string Code { get; set; }
        public string CityCode { get; set; }
        public string Name { get; set; }
        public string AlternateNames { get; set; }
        public long Lat { get; set; }
        public long Lon { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
        public int GmtOffset { get; set; }
        public int Popularity { get; set; }
        public bool IsRailroad { get; set; }
        public bool IsBusStation { get; set; }
        public string Icao { get; set; }
        public string Phone { get; set; }
        public string Site { get; set; }
        public string GeoNameId { get; set; }
        public int Routes { get; set; }
    }
}
