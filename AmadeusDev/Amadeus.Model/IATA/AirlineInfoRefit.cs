﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.Model.IATA
{
    public class AirlineInfoRefit
    {
        [JsonProperty(PropertyName = "iata_code")]
        public string Code { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        public AirlineInfo AsDomain()
        {
            return new AirlineInfo
            {
                Code = Code,
                Name = Name
            };
        }
    }
}
