﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.Model.IATA
{
    public class AirportResponseRefit
    {
        [JsonProperty(PropertyName = "response")]
        public List<AirportInfoRefit> Response { get; set; }
    }
}
