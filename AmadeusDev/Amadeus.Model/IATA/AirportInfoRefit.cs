﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.Model.IATA
{
    public class AirportInfoRefit
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        public AirportInfo AsDomain()
        {
            return new AirportInfo
            {
                Code = Code,
                Name = Name
            };
        }
    }
}
