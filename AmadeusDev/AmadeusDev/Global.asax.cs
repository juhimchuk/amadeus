﻿using Amadeus.ApiService.FlightInspirationSearch;
using Amadeus.ApiService.FlightLowFareSearch;
using Amadeus.ApiService.IATA;
using Amadeus.ApiService.OAuth;
using Amadeus.DAL.Repositories;
using Amadeus.IApiService;
using Amadeus.IApiService.IATA;
using Amadeus.IApiService.IFlightInspirationSearch;
using Amadeus.IApiService.IRepository;
using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AmadeusDev
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            IoCConfiguration();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ConfigurationManager.AppSettings["AmadeusToken"]  = new AmadeusOAuthService().GetAmadeusToken();
        }

        private void IoCConfiguration()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register the Autofac filter provider.
            builder.RegisterWebApiFilterProvider(config);

            // OPTIONAL: Register the Autofac model binder provider.
            builder.RegisterWebApiModelBinderProvider();
            builder.RegisterType<FlightSearch>().As<IFlightSearch>();
            //builder.RegisterType<RestIataCodesService>().As<IIataCodeService>()
            //       .WithParameter("baseUrl", ConfigurationManager.AppSettings["IATABaseUrl"])
            //       .WithParameter("apiKey", ConfigurationManager.AppSettings["IATAApiKey"]);
            //var iataService = new RestIataCodesService(ConfigurationManager.AppSettings["IATABaseUrl"], ConfigurationManager.AppSettings["IATAApiKey"]);
            //var cache = new CachingIataCodesService(iataService);
            //cache.Init().Wait();
            //builder.RegisterInstance<ICachingIataCodesService>(cache);
            builder.RegisterType<AiroportCodeRepository>().As<IAiroportCodeRepository>();
            builder.RegisterType<AmadeusOAuthService>().As<IAmadeusOAuthService>();
            builder.RegisterType<AiroportCodeService>().As<IAiroportCodeService>();
            builder.RegisterType<FlightLowFareSearch>().As<IFlightLowFareSearch>();
            var rep = new AiroportCodeRepository();
            var service = new AiroportCodeService(rep);
            var cache = new CachingAiroportCodeService(service);
            cache.Init().Wait();
            builder.RegisterInstance<ICachingAiroportCodeService>(cache);
            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
    
}
