﻿using Amadeus.IApiService.IFlightInspirationSearch;
using Domain.LowFareModels.ResponseModels;
using Domain.LowFareModels.SearchModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;

namespace AmadeusDev.Controllers
{
    public class FlightLowFareSearchController : ApiController
    {
        private readonly IFlightLowFareSearch _flightService;
        private string _token;

        public FlightLowFareSearchController()
        {
        }


        public FlightLowFareSearchController(IFlightLowFareSearch flightService)
        {
            _flightService = flightService;
            _token = ConfigurationManager.AppSettings["AmadeusToken"];
        }

        [Route("api/Search/Offers/")]
        public ResponseLowFareModel Get(string origin, string destination, DateTime departureDate, DateTime returnDate, int numberOfAdults, int numberOfChildren = 0, bool isNonStop = false)
        {
            var flights = _flightService.Search(new LowFareSearchModel
            {
                Token = _token,
                Origin = origin,
                Destination = destination,
                DepartureDate = GetDateFrom(departureDate),
                ReturnDate = GetDateFrom(returnDate),
                Passengers = GetPassengersInfo(numberOfAdults, numberOfChildren),
                IsNonStop = isNonStop
            });
            return flights;
        }

        private DateTime GetDateFrom(DateTime date)
        {
            return date.Date >= DateTime.Now.Date ? date : DateTime.Now;
        }

        private DateTime GetDateTo(DateTime date)
        {
            return date.Date >= DateTime.Now.Date && date.Date >= date.Date ? date : DateTime.Now;
        }

        List<Passenger> GetPassengersInfo(int numberOfAdults, int numberOfChildren)
        {
            var passengers = new List<Passenger>(2);

            if (numberOfAdults > 0)
            {
                passengers.Add(new Passenger
                {
                    NumberOfPassengers = numberOfAdults,
                    TypePassenger = TypePassenger.ADT
                });
            }

            if (numberOfChildren >= 0)
            {
                passengers.Add(new Passenger
                {
                    NumberOfPassengers = numberOfChildren,
                    TypePassenger = TypePassenger.CH
                });
            }

            return passengers;
        }
    }
}
