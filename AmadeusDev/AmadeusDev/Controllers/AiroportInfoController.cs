﻿using Amadeus.IApiService.IATA;
using Domain.GeneralModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AmadeusDev.Controllers
{
    public class AiroportInfoController : ApiController
    {
        private readonly IAiroportCodeService _infoService;
        private string _token;

        public AiroportInfoController()
        {
        }


        public AiroportInfoController(IAiroportCodeService infoService)
        {
            _infoService = infoService;
        }

        [Route("api/Info/")]
        public IEnumerable<AiroportCodeModel> Get()
        {
            return _infoService.GetAll();
        }
    }
}
