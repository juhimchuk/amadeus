﻿using Amadeus.IApiService.IFlightInspirationSearch;
using Domain.FlightInspirationModels.ResponseModels;
using Domain.FlightInspirationModels.SearchModels;
using System;
using System.Configuration;
using System.Web.Http;

namespace AmadeusDev.Controllers
{
    public class FlightInspirationSearchController : ApiController
    {
        private readonly IFlightSearch _flightService;
        private string _token;

        public FlightInspirationSearchController()
        {
        }


        public FlightInspirationSearchController(IFlightSearch flightService)
        {
            _flightService = flightService;
            _token = ConfigurationManager.AppSettings["AmadeusToken"];
        }

        [Route("api/Search/Inspiration/")]
        public FlightResponseModel Get(string origin, DateTime departureDateFrom, DateTime departureDateTo,bool isOneWay=false, bool isNonStop=false )
        {
            var res = _flightService.InspirationSearch(new SearchModel {
                Token = _token,
                Origin = origin,
                DepartureDateFrom = departureDateFrom.Date>=DateTime.Now.Date ? departureDateFrom : DateTime.Now,
                DepartureDateTo = departureDateTo.Date >= DateTime.Now.Date && departureDateTo.Date >= departureDateFrom.Date ? departureDateTo : DateTime.Now,
                IsOneWay = isOneWay,
                IsNonStop = isNonStop
            });

            return res;
        }

        [Route("api/Search/Cheapest/")]
        public FlightResponseModel Get(string origin, string destination, DateTime departureDateFrom, DateTime departureDateTo, bool isOneWay = false, bool isNonStop = false)
        {
            var res = _flightService.CheapestSearch(new SearchModel
            {
                Token = _token,
                Origin = origin,
                Destination = destination,
                DepartureDateFrom = GetDateFrom(departureDateFrom),
                DepartureDateTo = GetDateFrom(departureDateTo),
                IsOneWay = isOneWay,
                IsNonStop = isNonStop
            });
            return res;
        }

        private DateTime GetDateFrom(DateTime date)
        {
            return date.Date >= DateTime.Now.Date ? date : DateTime.Now;
        }

        private DateTime GetDateTo(DateTime date)
        {
            return date.Date >= DateTime.Now.Date && date.Date >= date.Date ? date : DateTime.Now;
        }
    }
}
