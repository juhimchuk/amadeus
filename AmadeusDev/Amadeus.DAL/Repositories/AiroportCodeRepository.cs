﻿using Amadeus.DAL.Entities;
using Amadeus.IApiService.IRepository;
using Domain.GeneralModels;
using System.Collections.Generic;
using System.Linq;

namespace Amadeus.DAL.Repositories
{
    public class AiroportCodeRepository : BaseRepository<AiroportCode>, IAiroportCodeRepository
    {
        public AiroportCodeRepository(): base(new AmadeusContext())
        {
        }

        public IEnumerable<AiroportCodeModel> GetAll()
        {
            return base.GetAll().Select(e=>Map(e));
        }

        private AiroportCodeModel Map(AiroportCode entity)
        {
            return new AiroportCodeModel
            {
                IataCode = entity.IataCode,
                AiroportName = entity.AiroportName,
                CityName = entity.CityName,
                CountryName = entity.CountryName
            };
        }
    }
}
