namespace Amadeus.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AiroportCodes",
                c => new
                    {
                        IataCode = c.String(nullable: false, maxLength: 128),
                        AiroportName = c.String(),
                        CityName = c.String(),
                        CountryName = c.String(),
                    })
                .PrimaryKey(t => t.IataCode);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AiroportCodes");
        }
    }
}
