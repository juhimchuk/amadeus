﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.DAL.Entities
{
    public class AiroportCode
    {
        [Key]
        public string IataCode { get; set; }
        public string AiroportName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
    }
}
