﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.DAL.Entities
{
    public class AmadeusContext : DbContext
    {
        public AmadeusContext() : base("name=AmadeusContext")
        {
        }

        public virtual DbSet<AiroportCode> AiroportCode { get; set; }

    }
}
