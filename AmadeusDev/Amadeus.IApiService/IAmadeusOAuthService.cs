﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.IApiService
{
    public interface IAmadeusOAuthService
    {
        string GetAmadeusToken();
    }
}
