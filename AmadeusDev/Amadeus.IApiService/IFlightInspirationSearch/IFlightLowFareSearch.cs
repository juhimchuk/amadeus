﻿using Domain.LowFareModels.ResponseModels;
using Domain.LowFareModels.SearchModels;

namespace Amadeus.IApiService.IFlightInspirationSearch
{
    public interface IFlightLowFareSearch
    {
        ResponseLowFareModel Search(LowFareSearchModel search);
    }
}
