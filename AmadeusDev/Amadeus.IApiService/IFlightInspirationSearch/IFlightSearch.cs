﻿using Domain.FlightInspirationModels.ResponseModels;
using Domain.FlightInspirationModels.SearchModels;

namespace Amadeus.IApiService.IFlightInspirationSearch
{
    public interface IFlightSearch
    {
        FlightResponseModel InspirationSearch(SearchModel search);

        FlightResponseModel CheapestSearch(SearchModel search);
    }
}
