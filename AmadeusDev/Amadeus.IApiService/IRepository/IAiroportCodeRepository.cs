﻿using Domain.GeneralModels;
using System.Collections.Generic;

namespace Amadeus.IApiService.IRepository
{
    public interface IAiroportCodeRepository
    {
        IEnumerable<AiroportCodeModel> GetAll();
    }
}
