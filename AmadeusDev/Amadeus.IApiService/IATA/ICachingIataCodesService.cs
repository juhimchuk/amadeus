﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.IApiService.IATA
{
    public interface ICachingIataCodesService
    {
        Task Init();

        string GetAirportName(string code);

        string GetAirlineName(string code);

        string GetCityName(string code);
    }
}
