﻿using Domain.GeneralModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.IApiService.IATA
{
    public interface IAiroportCodeService
    {
        IEnumerable<AiroportCodeModel> GetAll();
    }
}
