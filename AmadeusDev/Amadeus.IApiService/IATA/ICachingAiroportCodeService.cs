﻿using Domain.GeneralModels;
using System.Threading.Tasks;

namespace Amadeus.IApiService.IATA
{
    public interface ICachingAiroportCodeService
    {
        Task Init();
        AiroportCodeModel GetInfo(string code);
    }
}
