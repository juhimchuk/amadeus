﻿using Amadeus.Model.IATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.IApiService.IATA
{
    public interface IIataCodeService
    {
        Task<AirportInfo> GetAirportInfoByCode(string airportCode);
        Task<CityInfo> GetCityInfoByCode(string cityCode);
        Task<Dictionary<string, AirportInfo>> GetAllAirportInfos();
        Task<Dictionary<string, CityInfo>> GetAllCityInfos();
        Task<Dictionary<string, AirlineInfo>> GetAllAirlinesInfos();
    }
}
