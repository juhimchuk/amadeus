﻿using Amadeus.Model.IATA;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amadeus.IApiService.IATA
{
    public interface IIataCodesServiceRefit
    {
        [Get("/api/v6/airports?api_key={apiKey}&code={code}")]
        Task<AirportResponseRefit> GetAirportInfos(string apiKey, string code);

        [Get("/api/v6/cities?api_key={apiKey}&code={code}")]
        Task<CityInfoResponse> GetCityInfos(string apiKey, string code);

        [Get("/api/v6/airports?api_key={apiKey}")]
        Task<AirportResponseRefit> GetAllAirportInfos(string apiKey);

        [Get("/api/v6/cities?api_key={apiKey}")]
        Task<CityInfoResponse> GetAllCityInfos(string apiKey);

        [Get("/api/v7/airlines?api_key={apiKey}")]
        Task<AirlineResponseRefit> GetAllAirlinesInfos(string apiKey);
    }
}
