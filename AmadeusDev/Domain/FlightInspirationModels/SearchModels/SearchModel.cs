﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.FlightInspirationModels.SearchModels
{
    public class SearchModel
    {
        public string Origin { get; set; }

        public string Destination { get; set; }

        public string Token { get; set; }

        public DateTime DepartureDateFrom { get; set; }

        public DateTime DepartureDateTo { get; set; }

        public bool IsOneWay { get; set; }

        public bool IsNonStop { get; set; }

    }
}
