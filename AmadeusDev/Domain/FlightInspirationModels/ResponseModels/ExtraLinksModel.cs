﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.FlightInspirationModels.ResponseModels
{
    public class ExtraLinksModel
    {
        public string SearchDate { get; set; }

        public string FlightDestinations { get; set; }

        public string SearchTickets { get; set; }
    }
}
