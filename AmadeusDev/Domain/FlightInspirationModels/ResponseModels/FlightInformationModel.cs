﻿using Domain.GeneralModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.FlightInspirationModels.ResponseModels
{
    public class FlightInformationModel
    {
        public string Type { get; set; }

        public AiroportModel Origin { get; set; }

        public AiroportModel Destination { get; set; }

        public DateTime DepartureDate { get; set; }

        public DateTime? ReturnDate { get; set; }

        public decimal Price { get; set; }

        public ExtraLinksModel Links { get; set; }
    }
}
