﻿using Domain.GeneralModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.FlightInspirationModels.ResponseModels
{
    public class FlightResponseModel
    {
        public IEnumerable<FlightInformationModel> Data { get; set; }

        public IEnumerable<ResponseErrorModel> Errors { get; set; }
    }
}
