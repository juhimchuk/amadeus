﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.SearchModels
{
    public class LowFareSearchModel
    {
        public string Origin { get; set; }

        public string Token { get; set; }

        public string Destination { get; set; }

        public DateTime DepartureDate { get; set; }

        public DateTime ReturnDate { get; set; }

        public List<Passenger> Passengers { get; set; }

        public bool IsNonStop { get; set; }

    }
}
