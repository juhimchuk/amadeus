﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.SearchModels
{
    public class Passenger
    {
        public int NumberOfPassengers { get; set; }
        public TypePassenger TypePassenger { get; set; }
    }
}
