﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class AirCraftModel
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}
