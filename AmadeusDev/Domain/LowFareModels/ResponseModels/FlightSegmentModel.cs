﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class FlightSegmentModel
    {
        public LowFareAiroportModel Departure { get; set; }
        
        public LowFareAiroportModel Arrival { get; set; }
        
        public AirCraftModel AirCraft { get; set; }
        
        public OperatingCodeModel Operating { get; set; }
        
    }
}
