﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class SegmentModel
    {
        public FlightSegmentModel FlightSegment { get; set; }
        
        public PricingDetailModel PricingDetailPerAdult { get; set; }
        
        public PricingDetailModel PricingDetailPerChild { get; set; }
    }
}
