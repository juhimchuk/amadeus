﻿using Domain.GeneralModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class ResponseLowFareModel
    {
        public IEnumerable<DataModel> Data { get; set; }

        public IEnumerable<ResponseErrorModel> Errors { get; set; }
    }
}
