﻿using Domain.GeneralModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class LowFareAiroportModel
    {
        public AiroportModel Airoport { get; set; }

        public int? Terminal { get; set; }

        public DateTime ArrivalTime { get; set; }
    }
}
