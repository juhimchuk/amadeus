﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class OperatingCodeModel
    {
        public string CarrierCode { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }
    }
}
