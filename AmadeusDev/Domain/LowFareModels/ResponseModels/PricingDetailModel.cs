﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class PricingDetailModel
    {
        public string TravelClass { get; set; }

        public string FareClass { get; set; }

        public int Availability { get; set; }

        public string FareBasis { get; set; }

    }
}
