﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class PriceModel
    {
        public decimal Total { get; set; }

        public decimal TotalTaxes { get; set; }
    }
}
