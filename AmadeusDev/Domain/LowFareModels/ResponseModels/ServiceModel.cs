﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class ServiceModel
    {
        public IEnumerable<SegmentModel> Segments { get; set; }
    }
}
