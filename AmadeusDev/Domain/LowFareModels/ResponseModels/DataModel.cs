﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class DataModel
    {
        public string Type { get; set; }
        
        public string Id { get; set; }
        
        public IEnumerable<OfferItemsModel> OfferItems { get; set; }
    }
}
