﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.LowFareModels.ResponseModels
{
    public class OfferItemsModel
    {
        public IEnumerable<ServiceModel> Sevices { get; set; }

        public PriceModel Price { get; set; }
        
        public PriceModel PricePerAdult { get; set; }
        
        public PriceModel PricePerChild { get; set; }
    }
}
