﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.GeneralModels
{
    public class CityModel
    {
        public string Name { get; set; }
        public string CountryName { get; set; }
    }
}
