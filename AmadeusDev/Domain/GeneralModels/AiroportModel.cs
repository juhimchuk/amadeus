﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.GeneralModels
{
    public class AiroportModel
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public CityModel City { get; set; }
    }
}
