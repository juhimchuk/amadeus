﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.GeneralModels
{
    public class AiroportCodeModel
    {
        public string IataCode { get; set; }
        public string AiroportName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
    }
}
