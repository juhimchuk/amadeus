﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.GeneralModels
{
    public class ResponseErrorModel
    {
        public int Status { get; set; }

        public int Code { get; set; }

        public string Title { get; set; }

        public string Detail { get; set; }
    }
}
